package com.bgiaa.flash_card_android;

import android.os.Parcel;
import android.os.Parcelable;

public class FlashCardAnswer implements Parcelable {
    public String sentence;
    public boolean is_right;

    public FlashCardAnswer(String sentence, boolean is_right) {
        this.sentence = sentence;
        this.is_right = is_right;
    }

    protected FlashCardAnswer(Parcel in) {
        sentence = in.readString();
        is_right = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sentence);
        dest.writeByte((byte) (is_right ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FlashCardAnswer> CREATOR = new Creator<FlashCardAnswer>() {
        @Override
        public FlashCardAnswer createFromParcel(Parcel in) {
            return new FlashCardAnswer(in);
        }

        @Override
        public FlashCardAnswer[] newArray(int size) {
            return new FlashCardAnswer[size];
        }
    };
}
