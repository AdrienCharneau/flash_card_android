package com.bgiaa.flash_card_android;

public class JSONHandler {


    public static int mapsImagetoDrawable(String imgName) {

        switch (imgName) {
            case "aa.jpg":
                return R.drawable.aa;
            case "ar.jpg":
                return R.drawable.ar;
            case "cinqcinq.jpg":
                return R.drawable.cinqcinq;
            case "deuxtrois.jpg":
                return R.drawable.deuxtrois;
            case "dixv.jpg":
                return R.drawable.dixv;
            case "dixv_suite.jpg":
                return R.drawable.dixv_suite;
            case "dv.jpg":
                return R.drawable.dv;
            default:
                return 0;
        }
    }
}
