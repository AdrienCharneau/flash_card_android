package com.bgiaa.flash_card_android.api;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PokerAPI {

    @GET("pokerhands/")
    Call<List<QuestionWrapper>> getQuestions();

    @GET("pokerhands/")
    Call<List<QuestionWrapperDifficulty>> getQuestionsbyDifficulty(@Query("difficulty") String difficulty);

}
