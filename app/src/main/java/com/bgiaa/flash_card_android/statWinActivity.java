package com.bgiaa.flash_card_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.lang.Math;

public class statWinActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stat_win);

        Intent intent = getIntent();
        int totalCards = intent.getIntExtra("TotalCards", 0);
        int wins  = intent.getIntExtra("wins", 0);
        String difficulty = intent.getStringExtra("Difficulty");

        final TextView winText = findViewById(R.id.saussageId);
        final TextView winNumber = findViewById(R.id.winId);
        final TextView total = findViewById(R.id.totalID);
        final TextView difficultyrino = findViewById(R.id.difficultyTxt);
        final TextView winRate = findViewById(R.id.winRate);
        final Button menu = findViewById(R.id.menuBtnId);


        winNumber.setText(""+wins);
        total.setText(""+totalCards);
        int lewinrate = Math.round(( (float)wins / totalCards ) * 100);
        winRate.setText(lewinrate+" %");
        difficultyrino.setText(difficulty);

        if (lewinrate == 100)
        {
            winText.setText("Félicitation, vous êtes un vrai de joueur de poker !");
        }
        else if (lewinrate == 0)
        {
            winText.setText("Vous êtes clairement naze, désinstallez l'appli svp");
        }
        else
            winText.setText("Pas ouf, mais pas nul non plus");

        menu.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menuBtnId:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }

    }
}
