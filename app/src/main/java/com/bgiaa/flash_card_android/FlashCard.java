package com.bgiaa.flash_card_android;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class FlashCard implements Parcelable {
    private int combiImage;
    private String question;
    private String difficulty;
    private ArrayList<FlashCardAnswer> flashCardAnswerList;

    public FlashCard(int combiImage, String question, String difficulty, ArrayList<FlashCardAnswer> flashCardAnswerList) {
        this.combiImage = combiImage;
        this.question = question;
        this.difficulty = difficulty;
        this.flashCardAnswerList = flashCardAnswerList;
    }

    protected FlashCard(Parcel in) {
        combiImage = in.readInt();
        question = in.readString();
        difficulty = in.readString();
        flashCardAnswerList = in.createTypedArrayList(FlashCardAnswer.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(combiImage);
        dest.writeString(question);
        dest.writeString(difficulty);
        dest.writeTypedList(flashCardAnswerList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FlashCard> CREATOR = new Creator<FlashCard>() {
        @Override
        public FlashCard createFromParcel(Parcel in) {
            return new FlashCard(in);
        }

        @Override
        public FlashCard[] newArray(int size) {
            return new FlashCard[size];
        }
    };

    public int getCombiImage() {
        return combiImage;
    }

    public String getQuestion() {
        return question;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public ArrayList<FlashCardAnswer> getFlashCardAnswerList() {
        return flashCardAnswerList;
    }
}
