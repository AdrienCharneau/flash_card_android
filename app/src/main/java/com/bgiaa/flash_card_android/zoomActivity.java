package com.bgiaa.flash_card_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class zoomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);

        final ImageView image = findViewById(R.id.handImageZoom);
        final ImageButton closeButton = findViewById(R.id.closeImageButton);

        Intent intent = getIntent();
        int imageId = intent.getIntExtra("ZoomImageCard", 0);
        image.setImageResource(imageId);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
