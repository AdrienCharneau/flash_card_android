package com.bgiaa.flash_card_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class CardActivity extends AppCompatActivity {
    //  Init Counter Variables
    private int cardCounterMax;
    private int cardCounterCurrent;
    private int totalWins;

    //  Init Object Variables
    private TextView cardCounterTextView;
    private TextView difficultyTextView;
    private ImageButton cardHandImageButton;
    private TextView questionTextView;
    private RadioGroup answersRadioGroup;
    private RadioButton radioButton_1;
    private RadioButton radioButton_2;
    private RadioButton radioButton_3;
    private RadioButton selectedRadioButton;
    private Button validateButton;


    private ArrayList<FlashCard> cardList = new ArrayList<FlashCard>();

    FlashCard flashCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        Intent intent = getIntent();
        cardCounterCurrent = intent.getIntExtra("CurrentCounter", 0);
        totalWins = intent.getIntExtra("TotalWins", 0);

        cardList = getIntent().getParcelableArrayListExtra("cards");
        /*
        if(cardCounterCurrent < 1) {
            cardList = getIntent().getParcelableArrayListExtra("cards");
        }else {
            cardList = getIntent().getParcelableArrayListExtra("cardsList");
        }
        */

        cardCounterMax = cardList.size()-1;
        flashCard = cardList.get(cardCounterCurrent);

        //  Linking Widgets
        cardCounterTextView = findViewById(R.id.indexTextView);
        difficultyTextView = findViewById(R.id.difficultyTextView);
        cardHandImageButton = findViewById(R.id.cardHandImageView);
        questionTextView = findViewById(R.id.questionTextView);
        answersRadioGroup = findViewById(R.id.answersRadioGroup);
        radioButton_1 = findViewById(R.id.radio_1);
        radioButton_2 = findViewById(R.id.radio_2);
        radioButton_3 = findViewById(R.id.radio_3);
        validateButton = findViewById(R.id.validateAnswerButton);

        //  Randomize Flash card answers
        Collections.shuffle(flashCard.getFlashCardAnswerList());

        //  Setting up Widgets
        cardCounterTextView.setText( (cardCounterCurrent+1) + "/" + (cardCounterMax+1) );
        difficultyTextView.setText(flashCard.getDifficulty());
        cardHandImageButton.setImageResource(flashCard.getCombiImage());
        questionTextView.setText(flashCard.getQuestion());
        radioButton_1.setText(flashCard.getFlashCardAnswerList().get(0).sentence);
        radioButton_2.setText(flashCard.getFlashCardAnswerList().get(1).sentence);
        radioButton_3.setText(flashCard.getFlashCardAnswerList().get(2).sentence);

        //  Card hand Zoom interaction@
        cardHandImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZoomCardHandImageIntent();
            }
        });

        //  Validate answer/Next Question
        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedRadioId = answersRadioGroup.getCheckedRadioButtonId();
                String answerDisplayMessage;
                selectedRadioButton = findViewById(selectedRadioId);
                String goodAnswer = FindGoodAnswer();

                if(selectedRadioButton.getText().equals(goodAnswer)){
                    answerDisplayMessage = "Bonne réponse!";
                    totalWins++;
                }
                else{
                    answerDisplayMessage = "Mauvaise réponse! Bonne réponse: " + goodAnswer;
                }
                for (int i = 0; i < answersRadioGroup.getChildCount(); i++) {
                    answersRadioGroup.getChildAt(i).setEnabled(false);
                }
                validateButton.setText("Question Suivante");
                validateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NextCardIntent();
                    }
                });

                Toast toastTest = Toast.makeText(CardActivity.this, answerDisplayMessage, Toast.LENGTH_SHORT);
                toastTest.setGravity(Gravity.BOTTOM, 0, 150);
                toastTest.show();
                //Toast.makeText(CardActivity.this, answerDisplayMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ZoomCardHandImageIntent(){
        Intent intent = new Intent(CardActivity.this, zoomActivity.class);
        intent.putExtra("ZoomImageCard", flashCard.getCombiImage());
        startActivityForResult(intent, 1);
    }

    private void NextCardIntent(){
        if(cardCounterCurrent < cardCounterMax){
            for (int i = 0; i < answersRadioGroup.getChildCount(); i++) {
                answersRadioGroup.getChildAt(i).setEnabled(true);
            }
            cardCounterCurrent++;
            Intent intent = new Intent(CardActivity.this, CardActivity.class);
            intent.putParcelableArrayListExtra("cards", cardList);
            intent.putExtra("CurrentCounter", cardCounterCurrent);
            intent.putExtra("TotalWins", totalWins);
            startActivity(intent);
            finish();
        }
        else
        {
            Intent intent = new Intent(CardActivity.this, statWinActivity.class);
            intent.putExtra("TotalCards", (cardCounterMax + 1));
            intent.putExtra("wins", totalWins);
            intent.putExtra("Difficulty", flashCard.getDifficulty());
            startActivity(intent);
            finish();
        }
    }

    private String FindGoodAnswer() {
        String answer = "";
        for (FlashCardAnswer f: flashCard.getFlashCardAnswerList()) {
            if(f.is_right == true ) {
                answer = f.sentence;
            }
        }
        return answer;
    }

    public void checkButton(View v) {

    }
}
